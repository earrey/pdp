import json
from shapely.geometry import LineString, Point
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from geopy.distance import great_circle

# Read json
data = json.load(open('anf_data.json', encoding='utf-8-sig'))
s = json.load(open('Crimenes_anf01.geojson',encoding='utf-8-sig'))

crimes=[]
for point in s['features']:
    for i in point['geometry']['coordinates']:
        if i not in crimes:
            crimes.append(i)

with open('crimesMinic.json','w') as file:
    json.dump(crimes,file)

Block = data['Block']
link = data['keys']

plt.figure(figsize=(20, 20))
ax = plt.axes(projection=ccrs.PlateCarree())

for i in link:
    a, b = [], []
    for lon, lat in Block[i]:
        a.append(lon)
        b.append(lat)
    plt.plot(a, b, color='black', alpha=0.8)

w, z = [], []
for i,point in enumerate(crimes):
    w.append(crimes[i][0])
    z.append(crimes[i][1])
plt.plot(w, z, marker='.', markersize=3.0, markeredgewidth=0.5, markerfacecolor='red', color='none')
plt.savefig('demanda_anf.pdf')

lat_min, lat_max = -23.74, -23.51
lon_min, lon_max = -70.45, -70.36

crimes_x_y=[]
for i, point in enumerate(crimes):
    lat,lon=[],[]
    lat = crimes[i][0]
    lon = crimes[i][1]
    dist_x = great_circle((lat_min,lon_min),(lat_min,lon)).meters
    dist_y = great_circle((lat_min,lon_min),(lat,lon_min)).meters
    crimes_x_y.append([dist_x,dist_y])

Block_x_y = []
for i in range(len(Block)):
    b=[]
    for point in Block[i]:
        lat, lon = [], []
        lat = point[0]
        lon = point[1]
        dist_x = great_circle((lat_min, lon_min), (lat_min, lon)).meters
        dist_y = great_circle((lat_min, lon_min), (lat, lon_min)).meters
        b.append([dist_x, dist_y])
    Block_x_y.append(b)

print(len(Block_x_y))

dis=[]
D={}
for i,crime in enumerate(crimes_x_y):
    d=[]
    min=9999999
    for j in range(len(Block)):
        Pto = crime
        line = Block_x_y[j]
        p = Point(Pto)
        linea = LineString(line)
        a = p.distance(linea)
        dis.append({'crime':i,'link':j,'d':a})
        if a<min:
            min=a
            b=j
    d.append({'link':b,'d':min})
    D[i]=d

with open('distancesCrimes_antofagasta.json','w') as file:
    json.dump(dis,file)
with open('MinimDisCrimes_antofagasta.json','w') as file:
    json.dump(D,file)

demand={}
for i in link:
    c=0
    for n in D:
        if i==D[n][0]['link']:
            c+=1
    demand[i]=c

with open('Demand_antofagasta.json','w') as file:
    json.dump(demand,file)

