import json

#Read json files
s = json.load(open('anf_M1Sol01.json',encoding='utf-8-sig'))
data = json.load(open('New_Data.json',encoding='utf-8-sig'))

dist={}
for n in data['Distance']:
    i=int(n['i'])
    j=int(n['j'])
    d=float(n['d'])
    dist[i,j]=d

#Select medians
M = {}
for k in s:
    links = {}
    for i in s[k]:
        distance=[]
        m=0
        for j in s[k]:
            if i != j:
                m += dist[i, j]
        distance.append(m)
        links[i]=distance
    M[k] = links

Median=[]
for k in M:
    min_value = 99999999999999
    min_key = 0
    for i in M[k]:
        if min_value > M[k][i][0]:
            min_value = M[k][i][0]
            min_key = i
    Median.append(min_key)

#Save initial Solution
active=[]
for k in s:
    for i in s[k]:
            active.append([i,Median[int(k)]])
S={}
S['Initial Solution']= active
with open('anf_InitialSolP01.json','w') as file:
    json.dump(S,file)


