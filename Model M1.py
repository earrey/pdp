from gurobipy import *
mdl = Model("Prueba")
import json

# Read json files
data = json.load(open('New_Data.json', encoding='utf-8-sig'))
C = json.load(open('C_01.json', encoding='utf-8-sig'))
D=json.load(open('Demand_01.json',encoding='utf-8-sig' ))
dist = {}
for n in data['Distance']:
    i = int(n['i'])
    j = int(n['j'])
    d = float(n['d'])
    dist[i, j] = d

# Model
print('Solving, please wait')
link = data['keys']
arcos = [(i, j) for i in link for j in link]
district = [k for k in range(C)]
print(district)

#Variables
x = mdl.addVars({(i, k) for i in link for k in district}, vtype=GRB.BINARY, name='x')
z = mdl.addVar(vtype=GRB.CONTINUOUS, name='z')
print('Solving, please wait')

#Objective function
mdl.setObjective(z, GRB.MINIMIZE)

#Constraints
print('Solving, please wait: Constrain c1')
for i in link:
    mdl.addConstr(quicksum(x[(i, k)] for k in district) == 1, name='c1')

print('Solving, please wait: Constrain c2')
for k in district:
    mdl.addConstr(z >= quicksum(D[str(i)] * x[(i, k)] for i in link), name='c2')

mdl.optimize()
mdl.write("anf_M1Sol01.sol")
print('z_anf=', z.x)

#Save Solution
Z = {}
Z = z.x
with open('anf_z01.json', 'w') as file:
    json.dump(Z, file)
Solution = {}
for k in district:
    b = []
    for i in link:
        if x[(i, k)].x > 0.9:
            b.append(i)
    Solution[k] = b
with open('anf_M1Sol01.json', 'w') as file:
    json.dump(Solution, file)

