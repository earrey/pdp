import json
from medoid import k_medoids

data = json.load(open('anf_data.json', encoding='utf-8-sig'))
dem=json.load(open('Demand_antofagasta.json',encoding='utf-8-sig' ))
p = json.load(open('Capacity_antofagasta.json', encoding='utf-8-sig'))

factor=1
demand_max= p['Capacity']*factor
C=p['K']
points=data['keys']

dist={}
for n in data['Distance']:
    i=int(n['i'])
    j=int(n['j'])
    d=float(n['d'])
    dist[i,j]=d

def distance(a,b):
    return(dist[a,b])

def demand(a):
    return(dem[str(a)])

diameter, capacity, Distancia, medoids = k_medoids(points, demand=demand, k=C, distance=distance, spawn=3)

medianas=[medoids.__getitem__(i).kernel for i in range(C)]
with open('medianas_anf.json','w') as file:
    json.dump(medianas,file)

solution=[]
for m in range(C):
    links=list(medoids.__getitem__(m).elements)
    for i in links:
        solution.append([i,medianas[m]])

print(len(solution))
print(capacity)
S={}
S['Initial Solution']= solution
with open('Initial_medoids_anf.json','w') as file:
    json.dump(S,file)


#SECODN PHASE
import random
from operator import itemgetter

def Calculate_capacity(self,demand):
    return (sum(demand(a) for a in self))

Districts ={(m.kernel,m.Total):m.elements for m in medoids}
Max_Kernel, Max_capacity = max((n for n in Districts), key=itemgetter(1))
Min_Kernel, Min_capacity, =  min((n for n in Districts), key=itemgetter(1))

while Max_capacity > demand_max:
    for p in random.sample(Districts[Max_Kernel,Max_capacity], 3):
        Districts[Max_Kernel, Max_capacity].remove(p)
        Districts[Min_Kernel, Min_capacity].append(p)

    new_cap_max = Calculate_capacity(Districts[Max_Kernel, Max_capacity], demand)
    Districts[Max_Kernel,new_cap_max] = Districts.pop((Max_Kernel,Max_capacity))
    new_cap_min = Calculate_capacity(Districts[Min_Kernel, Min_capacity],demand)
    Districts[Min_Kernel, new_cap_min] = Districts.pop((Min_Kernel, Min_capacity))

    Max_Kernel, Max_capacity = max((n for n in Districts), key=itemgetter(1))
    Min_Kernel, Min_capacity, = min((n for n in Districts), key=itemgetter(1))
    print(Max_capacity)

solution2=[]
for m in Districts:
    for link in Districts[m]:
        solution2.append([link,m[0]])

S2={}
S2['Initial Solution']= solution2
with open('Initial_medoids_anf.json','w') as file:
    json.dump(S2,file)
