import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import json

plt.figure(figsize=(10, 10))
ax = plt.axes(projection=ccrs.PlateCarree())

data = json.load(open('New_Data.json', encoding='utf-8-sig'))
C = json.load(open('C_01.json', encoding='utf-8-sig'))
Solution = json.load(open('anf_M1Sol01.json',encoding='utf-8-sig'))
Solution1 = json.load(open('anf_PmedianSol01.json', encoding='utf-8-sig'))
medianas = json.load(open('anf_medianas01.json', encoding='utf-8-sig'))
Blocks = data['Block']

for link in Solution1[str(medianas[0])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='blue', alpha=1)
    #plt.annotate(link, (np.mean(a), np.mean(b)))

for link in Solution1[str(medianas[1])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='red', alpha=1)
    #plt.annotate(link, (np.mean(a), np.mean(b)))

plt.savefig('PmedianSol.png')
plt.show()

for link in Solution1[str(medianas[2])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='green', alpha=1)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in Solution1[str(medianas[3])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='yellow', alpha=1)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in Solution1[str(medianas[4])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='pink', alpha=1)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in Solution1[str(medianas[5])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='cyan', alpha=1)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in Solution1[str(medianas[6])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='black', alpha=1)
    #plt.annotate(link,(np.mean(a),np.mean(b)))




