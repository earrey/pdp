import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import json

plt.figure(figsize=(10, 10))
ax = plt.axes(projection=ccrs.PlateCarree())

data = json.load(open('anf_data.json', encoding='utf-8-sig'))
p = json.load(open('Capacity_antofagasta.json', encoding='utf-8-sig'))
Solution1 = json.load(open('Initial_medoids_anf.json', encoding='utf-8-sig'))
#medianas = json.load(open('medianas_01.json', encoding='utf-8-sig'))
medianas = [8874, 5112, 6711, 11334, 9364, 9718, 3876, 7463, 5880, 4538]


Blocks = data['Block']

C=p['K']

# Plot
for link in Solution1[str(medianas[0])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='blue', alpha=1)
    #plt.annotate(link, (np.mean(a), np.mean(b)))

for link in Solution1[str(medianas[1])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='red', alpha=1)
    #plt.annotate(link, (np.mean(a), np.mean(b)))



for link in Solution1[str(medianas[2])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='green', alpha=1)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in Solution1[str(medianas[3])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='yellow', alpha=1)
    #plt.annotate(link,(np.mean(a),np.mean(b)))
plt.savefig('PmedianSol_minic_2.png')
plt.show()

for link in Solution1[str(medianas[4])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='pink', alpha=1)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in Solution1[str(medianas[5])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='cyan', alpha=1)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in Solution1[str(medianas[6])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='black', alpha=1)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

plt.savefig('Prueba_medodis.png')
plt.show()
