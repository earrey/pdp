from gurobipy import *
mdl = Model("Prueba")
import json

# Read json
data = json.load(open('anf_data.json', encoding='utf-8-sig'))
s = json.load(open('anf_InitialSolP01.json',encoding='utf-8-sig'))
z_value = json.load(open('anf_z01.json',encoding='utf-8-sig'))
P = json.load(open('C_01.json', encoding='utf-8-sig'))
D=json.load(open('Demand_01.json',encoding='utf-8-sig' ))

dist = {}

for n in data['Distance']:
    i=int(n['i'])
    j=int(n['j'])
    d=float(n['d'])
    dist[i,j]=d
print('Start Solving')

#Pmedian
A = data['keys']
arcos = [(i, j) for i in A for j in A]
#beta=data['Neighbor']

x = mdl.addVars({(i, j) for i in A for j in A}, vtype=GRB.BINARY, name='x')
y = mdl.addVars({j for j in A}, vtype=GRB.BINARY, name='y')

mdl.setObjective(quicksum(dist[(i, j)] * x[(i, j)] for i in A for j in A), GRB.MINIMIZE)

for i in A:
    mdl.addConstr(quicksum(x[(i, j)] for j in A) == 1, name='c1')

#print('Constraint neighbors')
#for k in A:
    #    for i in A:
    #    if len(beta[str(i)]) > 0:
     #       mdl.addConstr(x[(i, k)] <= quicksum(x[(j, k)] for j in beta[str(i)]))


print('Constraint c2')
mdl.addConstr(quicksum(y[j]for j in A) == P, name='c2')

print('Constraint c3')
for i in A:
    for j in A:
        mdl.addConstr(x[(i, j)] <= y[j], name='c3')

print('Constraint c4')
for j in A:
    mdl.addConstr(z_value >= quicksum(D[str(i)]*x[(i, j)] for i in A), name='c4')

print('Solving....')

active = s['Initial Solution']
for par in range(len(active)):
    i = active[par][0]
    k = active[par][1]
    x[(i, k)].start = 1

mdl.Params.MIPGap = 0.05
mdl.optimize()
mdl.write("anf_PmedianSol01.sol")

#Save Initial Solution
Solution1 = {}
for j in A:
    if y[j].x > 0.9:
        b = []
        for i in A:
            if x[(i, j)].x > 0.9:
                b.append(i)
        Solution1[j] = b
medianas = []
for p in Solution1:
    if len(Solution1[p]) > 0:
        medianas.append(p)

with open('anf_medianas01.json', 'w') as file:
    json.dump(medianas, file)

with open('anf_PmedianSol01.json', 'w') as file:
    json.dump(Solution1, file)
