import json
from geopy.distance import great_circle

# Read json
u = json.load(open('ANF.geojson', encoding='utf-8-sig'))

# Extract all links
all_block = []
for block in u['features']:
    for link in block['geometry']['coordinates']:
        all_block.append(link)
# Extract all points
all_point = []
for block in u['features']:
    for link in block['geometry']['coordinates']:
        for i in link:
            all_point.append(i)
# Save duplicate points
duplicate_Point = []
unique_Point = []
for point in all_point:
    if point not in unique_Point:
        unique_Point.append(point)
    else:
        if i not in duplicate_Point:
            duplicate_Point.append(point)
# Broke Link
all_newBlock = []
for i, link in enumerate(all_block):
    lastPoint = None
    for j, point in enumerate(link):
        if point in duplicate_Point:
            if lastPoint is None:
                all_newBlock.append(link[:j + 1])
                lastPoint = j
            else:
                all_newBlock.append(link[lastPoint:j + 1])
                lastPoint = j
        else:
            if j == len(link) - 1:
                all_newBlock.append(link[lastPoint:j + 1])
# Clear Blocks
Block = []
for i, link in enumerate(all_newBlock):
    if len(link) > 1 and link not in Block:
        Block.append(link)
print(len(Block))

# Neighbors Links
print("preprocessing: Neighbors Links")
Neighbor = {i: [] for i in range(len(Block))}
for i, link in enumerate(Block):
    for j, link2 in enumerate(Block):
        if j > i:
            for n, point in enumerate(link):
                for m, point2 in enumerate(link2):

                    if point == point2:
                        Neighbor[i].append(j)
                        Neighbor[j].append(i)

# Clear Neighbors
No_Neighbor = []
n = list(Neighbor.keys())
for i in n:
    if len(Neighbor[i])== 0:
        No_Neighbor.append(i)
        Neighbor.pop(i)

keys = list(Neighbor.keys())
# Position Links
print("preprocessing: Position Links")
Positions = {}
for i, link in enumerate(Block):
    M = []
    for j, point in enumerate(link):

        M.append(point)
        Positions[i] = M

# Calculate lenght
print("preprocessing: Calculate lenght")
lenght = {}
for i, link in Positions.items():
    last = None
    d = 0
    for j in range(len(link)):

        if last == None:
            last = j
        else:
            lon1, lat1 = Positions[i][last]
            lon2, lat2 = Positions[i][j]
            last = j
            d += great_circle((lat1, lon1), (lat2, lon2)).meters
            lenght[i] = d

# Calculate distance
print("preprocessing: Calculate distance")
dist = []
for i, link1 in enumerate(Block):
    for j, link2 in enumerate(Block):

        if j>i:
            this_dist = min(great_circle(link1[0], link2[0]).meters, great_circle(link1[0], link2[-1]).meters,
                           great_circle(link1[-1], link2[-1]).meters, great_circle(link1[-1], link2[0]).meters)
            dist.append({"i":i,"j":j,"d":this_dist})
            dist.append({"i": j, "j": i, "d": this_dist})
for i, link1 in enumerate(Block):
    for j, link2 in enumerate(Block):
        if i==j:
            this_dist=0
            dist.append({"i": i, "j": j, "d": this_dist})


print("Total Blocks: ", len(Block))

#Create json files
data={}
data['Block'] = Block
data['Position'] = Positions
data['Lenght'] = lenght
data['Neighbor'] = Neighbor
data['NoNeighbors'] = No_Neighbor
data['Distance'] = dist
data['keys'] = keys

with open('antofagasta_.json','w') as file:
    json.dump(data,file)

print("Preprocessing Complete")
