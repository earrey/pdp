import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import json

plt.figure(figsize=(10, 10))
ax = plt.axes(projection=ccrs.PlateCarree())

data = json.load(open('anf_data.json', encoding='utf-8-sig'))
p = json.load(open('Capacity_antofagasta.json', encoding='utf-8-sig'))
Solution1 = json.load(open('Initial_medoids_anf.json', encoding='utf-8-sig'))
#medianas = json.load(open('medianas_01.json', encoding='utf-8-sig'))
medianas = [8874, 5112, 6711, 11334, 9364, 9718, 3876, 7463, 5880, 4538]

Blocks = data['Block']
C=p['K']

M={}
for m in medianas:
    d=[]
    for block in Solution1['Initial Solution']:
        if m==block[1]:
            d.append(block[0])
    M[m]=d

with open('Medoid_plot.json', 'w') as file:
    json.dump(M, file)

# Plot
for link in M[(medianas[0])]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='blue',linewidth=1, alpha=0.8)
    #plt.annotate(link, (np.mean(a), np.mean(b)))

for link in M[medianas[1]]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='red',linewidth=1, alpha=0.8)
    #plt.annotate(link, (np.mean(a), np.mean(b)))


for link in M[medianas[2]]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='green',linewidth=1, alpha=0.8)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in M[medianas[3]]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='gold',linewidth=1, alpha=0.8)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in M[medianas[4]]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='lightcoral',linewidth=1, alpha=0.8)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in M[medianas[5]]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='cyan',linewidth=1, alpha=0.8)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in M[medianas[6]]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='black',linewidth=1, alpha=0.8)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in M[medianas[7]]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='indigo',linewidth=1, alpha=0.8)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in M[medianas[8]]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='sienna',linewidth=1, alpha=0.8)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

for link in M[medianas[9]]:
    a, b = [], []
    for lon, lat in Blocks[link]:
        a.append(lon)
        b.append(lat)
        plt.plot(a, b, color='teal',linewidth=1, alpha=0.8)
    #plt.annotate(link,(np.mean(a),np.mean(b)))

plt.savefig('Prueba_medodis_plot_1.pdf')
plt.show()
